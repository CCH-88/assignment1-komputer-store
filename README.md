# Assignment 1 - Komputer Store

## Description

Noroff assigment 1 Komputer store 

Showing a komputer store from an API in a simpel HTML page with CSS-styling

## Installation 

Clone this repository

```
https://gitlab.com/CCH-88/assignment1-komputer-store.git
```

## License

You are to share 50% of the profit with the authors...

## Author
Christian Casper Hofma,


## Release History

Version 1 - 14-09-2022

## Acknowledgements

Thank you Piotr.
